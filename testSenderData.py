#!/usr/bin/env python3

import calendar

from datetime import datetime

def dateToTimestamp(dateInt):
    dateString = dateInt.split(" ")[2]
    year = dateString[:4]
    month = dateString[4:][:2]
    day = dateString[:8][6:]
    hour = dateString[:10][8:]
    min = dateString[:12][10:]
    date = datetime(int(year), int(month), int(day), int(hour), int(min), 0)
    return date

values = ""
f = open('valueDB', 'r')
for line in f:
        if not line.startswith("PTEC"):
            if not line.startswith("DATE"):
                values +="," + line.strip()
            else:
                values += ",TIME = " + str(calendar.timegm(dateToTimestamp(line).utctimetuple())) + "000000000"
        else:
             values += "," + line + "\n"
             
for line in values.split("\n"):
		t = ""
		for l in line.split(","):
			if l.startswith("TIME"):
				t = l.split(" ")[2]
			elif l != "":
				if l.startswith("PTEC"):
					if l.split(" ")[2].startswith("HP"):
						print(l.split(" ")[0] + " value=" + "1" + " " + t)
					elif l.split(" ")[2].startswith("HC"):
						print(l.split(" ")[0] + " value=" + "0" + " " + t)
				else:	
					print(l.split(" ")[0] + " value=" + l.split(" ")[2] + " " + t)
#Imax séparré de iinst (garder même nom que capture)
