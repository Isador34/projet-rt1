#!/bin/bash

awk 'BEGIN {FS="\n";RS="DATE = ";ORS=""} { x=1; while (x<NF) {print $x"\t"; x++} print "\n"}' conso.db |  sed -e 's/\t[A-Z0-9]* = /\t/g' > conso.csv

