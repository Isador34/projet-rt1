#!/usr/bin/perl

use strict;
use warnings;
use DateTime;  
use Date::Manip;
use RRDs;

# ordre des informations dans le fichier csv
my $DATE = 0;
my $HCHC = 1;
my $HCHP = 2;
my $IINST1 = 3;
my $IINST2 = 4;
my $IINST3 = 5;
my $IMAX1 = 6;
my $IMAX2 = 7;
my $IMAX3 = 8;
my $ISOUSC = 9;
my $PAPP = 10;
my $PMAX = 11;
my $PPOT = 12;
my $PTEC = 13;

open my $info, "conso.csv" or die "Could not open !";

my $cpt = 0;

# pour chaque ligne
while( my $line = <$info>)  {   
	
	my @array = split("\t", $line);
	my $ldc="";
	$cpt ++;
	
	# conversion de la date en timestamp
	#$array[$DATE] = UnixDate(ParseDate($array[$DATE]), "%s");
	
	# cangement de l'info HP/HC par un bolléen PTEC=1 si HP
	$array[$PTEC] =~ s/HP.*$/1/;
	$array[$PTEC] =~ s/HC.*$/0/;
	
	# construction de la ldc pour "rrdtool update"
	$ldc = UnixDate(ParseDate($array[$DATE]), "%s");
	$ldc .= ":$array[$HCHC]:$array[$HCHP]:";
	$ldc .= "$array[$IINST1]:$array[$IINST2]:$array[$IINST3]:";
	$ldc .= "$array[$IMAX1]:$array[$IMAX2]:$array[$IMAX3]:";
	$ldc .= "$array[$PAPP]:$array[$PMAX]:$array[$PTEC]";

	print "$cpt : $array[$DATE] => $ldc\n";
	
	# construction de la ldc pour "rrdtool update"
#	RRDs::update ("compteur.rrd","$ldc");
        
        #my $ERR="0";
        #$ERR=RRDs::error;
        
 #       if (defined RRDs::error) {
#			print "ERROR while updating".RRDs::error."\n";
#			my $kb=<>;
		}

}

close $info;

#rrdtool update all.rrd \
#           978301200:300:1:600:300    \


        
