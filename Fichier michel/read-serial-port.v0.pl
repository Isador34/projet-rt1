#!/usr/bin/perl

use strict;
use warnings;
use IO::Handle;

open( COM, "/dev/ttyUSB0") || die "Cannot read serial port : $!\n";
my @keys = qw/ISOUSC HCHC HCHP PTEC IINST1 IINST2 IINST3 IMAX1 IMAX2 IMAX3 ADIR1 ADIR2 ADIR3 PMAX PAPP PPOT/;
my %infos;
my ($end_of_frame) = (0);
while(( my $line = <COM> ) and ($end_of_frame == 0)){
    # do stuff here
    chomp $line;
    foreach my $key (@keys) 
    { 
	if ($line =~ /$key ([^ ]+)/) 
	{
	    if (defined $infos{$key}) { 
			$end_of_frame = 1;
			}
	    else { 
			$infos{$key} = $1; 
			}
	}
    }
}

foreach my $key (sort {$a cmp $b} keys %infos)
{
    print $key." = ".$infos{$key}."\n";
}
