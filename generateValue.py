#!/usr/bin/env python3

import random
import calendar

from datetime import datetime
from random import randrange

def randDate():
	year = random.randint(2011, 2016)
	month = random.randint(1, 12)
	day = random.randint(1, 28)
	hour = random.randint(0, 23)
	min = random.randint(0, 59)
	sec = random.randint(0, 59)
	date = datetime(year, month, day, hour, min, sec)
	return date


def generateMAmp():
	amp = random.uniform(0, 10)
	return str(round(amp,3))

def generatePhase():
        phase = randrange(1, 4, 1)
        return str(phase)


count = 0
while (count < 60):
	p = generatePhase()
	a = generateMAmp()
	d = randDate();
	print("phase" + p +" value=" + a + " " +  str(calendar.timegm(d.utctimetuple())) + "000000000")
	count = count + 1
